import csv
import re

fileName="batterystats1.txt"
file = open(fileName, "r")
txt = file.read()

searches = ["screen:", "cpu:", "camera:", "system_services:", "audio:", "mobile_radio:",
            "sensors:", "gnss:", "wifi:", "idle:"]
headerType1 = ['DEVICE','POWER USE TOTAL (mAh)','POWER USE APPS (mAh)','DURATION (m:s:ms)'] 
infoHeader = headerType1[1:]

def createCSVWriter(name):
    sheet = open(name+'.csv','w',newline='')
    return csv.writer(sheet)

def generateHeaderMA():
    ## cria header com todos os tipos de gastos e tipo de medição
    headerRow = []
    for s in searches:
        for he in infoHeader:
            headerRow = headerRow + [s[:-1] + ' ' + he]
    return headerRow

def searchKeywords(s):
    x = re.search(".*" + s + ".*", txt)
    return x.group().strip().split()

def generateTime(res):
    if(len(res)>=5):
        res[-3] = res[-3][:-1]
        res[-2] = res[-2][:-1]
        res[-1] = res[-1][:-2]
        return res[-3]+":"+res[-2]+":"+res[-1]
    return '-'

def writeCSVForMachineLearning():
    ## monta a planilha pra machine learning
    writer = createCSVWriter('dataMA')
    headerRow = generateHeaderMA()
    writer.writerow(headerRow)
    row = []
    for s in searches:
        res = searchKeywords(s)
        row = row + [res[1],res[3]]
        row.append(generateTime(res))
    writer.writerow(row)
    

def writeCSVTraditional():
    ## monta a planilha do jeito tradicional
    writer = createCSVWriter('data')
    writer.writerow(headerType1)
    for s in searches:
        res = searchKeywords(s)
        row = [res[0][:-1], res[1],res[3]]
        row.append(generateTime(res))
        writer.writerow(row)

writeCSVTraditional()
writeCSVForMachineLearning()